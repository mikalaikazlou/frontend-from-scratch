import React from 'react';
import { TodoItemModel } from '../../models/TodoItemModel';

interface TodoItemProps {
  item: TodoItemModel
}

export const TodoItem: React.FC<TodoItemProps> = ({ item, children}) => {
  // throw new Error('TEST');
  return (
    <div className='todo-item'>
      {item.value}
      {children}
    </div>
  )
};
