import React, { PureComponent } from 'react';
import map from 'lodash/map';
import size from 'lodash/size';
import { TodoItemModel } from '../../models/TodoItemModel';
import { TodoItem } from './TodoItem';

interface TodoListProps {
  items: Array<TodoItemModel>;
  onRemove: (item: TodoItemModel) => void;
}

interface TodoListState {
  error?: Error | null;
}

export class TodoList extends PureComponent<TodoListProps, TodoListState> {

  constructor(props: TodoListProps) {
    super(props);
    this.state = {};
  }

  componentDidCatch(error: Error, errorInfo: React.ErrorInfo): void {
    this.setState({ error });
  }

  render() {
    const { error } = this.state;

    if (error) {
      return (
        <>
          <h3>{error.message}</h3>
          {error?.stack && (<pre>{error?.stack}</pre>)}
        </>
      )
    }

    const { items, onRemove } = this.props;
    return (
      <section>
        <h2>TODO list: <small>(active todos: {size(items)})</small></h2>
        {map(items, (item) => (
          <TodoItem key={item.id} item={item}>
            <button onClick={() => onRemove(item)}>Remove</button>
          </TodoItem>)
        )}
      </section>
    )
  }
}
