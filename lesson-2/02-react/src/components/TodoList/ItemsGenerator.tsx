import React, { useRef, useState } from 'react';
import { TodoItemModel } from '../../models/TodoItemModel';

interface ItemsGeneratorProps {
  onCreate: (item: TodoItemModel) => void;
}

const createItem = (id: number, value?: string | null): TodoItemModel => {
  if (!value) {
    throw Error('Could not create empty todo item');
  }

  return { id, value };
};

export const ItemsGenerator: React.FC<ItemsGeneratorProps> = ({ onCreate }) => {
  const [id, setId] = useState(0);

  const input = useRef<HTMLInputElement>(null);

  const handleAddItem = () => {
    if (!input.current) {
      return;
    }

    try {
      const nextId = id + 1;
      const item = createItem(nextId, input.current?.value);
      onCreate(item);
      setId(nextId);
      input.current.value = '';
    } catch (e) {
      console.error(e);
    }

  };

  return (
    <section className='items-generator'>
      <small>created items count: {id}</small>
      <input ref={input} />
      <button onClick={handleAddItem}>Add</button>
    </section>
  )
};
