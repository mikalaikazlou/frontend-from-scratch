
export const helloWord = (document: Document, target: keyof HTMLElementTagNameMap) => {
  const collection = document.getElementsByTagName(target);
  if (collection.length === 0) {
    throw new Error(`could not find tag ${target}`);
  }

  const element: HTMLElement = collection.item(0);
  element.innerText = 'Hello World!';
};
