import { SystemOutput } from './SystemOutput';
import { helloWord } from './hello';

declare global {
  interface Window {
    logger: SystemOutput;
  }
}

window.logger = new SystemOutput('Custom Logger:');

interface AppOptions {
  targetTag: keyof HTMLElementTagNameMap;
}

const run = (document: Document, options: Partial<AppOptions> = {}) => {
  window.logger.verbose('Application started');

  const { targetTag } = options;

  try {
    helloWord(document, targetTag || 'body');
    window.logger.verbose('Greetings was successfully added');
  } catch (e) {
    window.logger.error(e);
  }

  window.logger.verbose('Application finished');
};

run(document, { targetTag: 'p' });
