
// Чистая функция

const applyDiscount = (price: number, discount: number): number => {
  return price - price * discount;
};

// Нечистая функция

const discount = 0.25;

const invalid_applyDiscount = (price: number): number => {
  return price - price * discount;
};



// Карирование

const createDiscountFn = (discount: number) => (price: number): number => {
  return price - price * discount;
};

const prices = [100, 500, 159.99];
const pricesWithDiscount = prices.map(createDiscountFn(0.4));



// Мемоизация

const sum = (a: number, b: number): number => a + b;
type SumFn = (a: number, b: number) => number;

const memoSum = (fn: SumFn): SumFn => {
  const results = new Map<string, number>();
  return (a: number, b: number): number => {
    const key = `${a}__${b}`;
    if (!results.has(key)) {
      results.set(key, fn(a, b));
    }
    return results.get(key) as number;
  }
};

const memoizedSum = memoSum(sum);

memoizedSum(1, 2);


// Data immutability
interface Item {
  id: number;
  title: string;
  price: number;
}

const incorrectDiscount = (item: Item, discount: number): Item => {
  item.price = item.price - item.price * discount;
  return item;
};

const correctDiscount = (item: Item, discount: number): Item => {
  const { price } = item;
  return {
    ...item,
    price: price - price * discount
  };
};
