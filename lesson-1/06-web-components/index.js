'use strict';

(function (document, $) {
  class TextChangeEvent extends Event {

    constructor(value) {
      super('text-change', { bubbles: true, cancelable: false, composed: false });
      this.text = value;
    }
  }

  class EditableText extends HTMLElement {

    edit = false;

    get onTextChange() {
      return this.getAttribute('onTextChange');
    }

    constructor() {
      super();
      this.$text = $('<div></div>').addClass('editable-text-content');
      this.$input = $('<textarea />').attr('type', 'text').addClass('editable-text-input');
      this.$text.text(this.innerText);
      this.innerText = '';

      this.$text.on('click', this.handleClick);
      this.$input.on('input', this.handleTextInput);
      this.$input.on('change', this.handleTextChange);
      this.$input.on('blur', this.handleTextChange);
      this.$input.on('keydown', this.handleKeydown);

      $(this).append(this.$text);
    }

    handleKeydown = (e) => {
      if (e.key === 'Enter') {
         this.handleTextChange();
      }
    };

    handleClick = () => {
      if (this.edit) {
        return;
      }
      this.$input.val(this.$text.text());
      this.$text.addClass('edit');
      $(this).append(this.$input);
      this.edit = true;
      this.$input.focus();
    };

    handleTextInput = () => {
      this.$text.text(this.$input.val());
    };

    handleTextChange = () => {
      if (!this.edit) {
        return;
      }

      this.edit = false;
      this.handleTextInput();
      this.$input.detach();
      this.$text.removeClass('edit');
      this.dispatchChange();
    };

    dispatchChange = () => {
      const event = new TextChangeEvent(this.$text.text());
      this.dispatchEvent(event);
    }

  }

  customElements.define('editable-text', EditableText);

  $(document).on('text-change', 'editable-text', (event) => {
    console.log(event.target, event.originalEvent.text);
  })
})(document, jQuery);
