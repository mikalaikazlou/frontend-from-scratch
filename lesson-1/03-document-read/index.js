

// Scope

const scoped1 = 'scoped1 defined globally';
const scoped2 = 'scoped2 defined globally';
const global = 'Global variable';

function f1() {
  const scoped1 = 'f1 scoped variable';
  const scoped2 = 'scoped2 defined in f1';

  function f2() {
    const scoped2 = 'f2 scoped variable';

    console.log(`f2 scope:`);
    console.log(`global:`, global);
    console.log(`scoped1:`, scoped1);
    console.log(`scoped2:`, scoped2);
    console.log(`f2 scope end`);
  }

  console.log(`f1 scope:`);
  console.log(`global:`, global);
  console.log(`scoped1:`, scoped1);
  console.log(`scoped2:`, scoped2);
  console.log(`f1 scope end`);
  f2();


  console.log(this);
}

console.log(`global scope:`);
console.log(`global:`, global);
console.log(`scoped1:`, scoped1);
console.log(`scoped2:`, scoped2);
console.log(`global scope end`);
f1();


const plainObject = {
  field1: 123,
  field2: 'test',
  field3: f1,
};

